
//Написать функцию getSum, которая будет высчитывать сумму чисел от нуля, до параметра, который мы в неё передаем.
let num = Number(prompt("Введите число"));

function isNumber(num) {
    return typeof num === 'number' && !isNaN(num);
}

function getSum (num,isNumber) {    
    if (isNumber(num) == true) {
        num = Math.abs(num);
        return num * (num+1) / 2;
    } else {
        return "Ошибка вычисления, т.к. введено не число";
    }
}

console.log (getSum(num,isNumber));

//Напишите функцию которая в качестве аргумента принимает в себя сумму кредита который хочет получить клиент и верните результат переплаты по кредиту:

const percent = prompt("Процент по кредиту");
const year = prompt("Количество лет");
const summ = prompt("Сумма кредита");

function overpayment (year, percent,summ) {
    return summ/100*percent*year;
}

console.log (overpayment(year, percent, summ));

//Написать функцию getSum которая принимает два целых числа a и b, которые могут быть положительными или отрицательными, найти сумму всех чисел между ними, включая их, и вернуть ее. Если два числа равны, верните a или b.

let numOne = Number(prompt("Введите первое число"));
const numTwo = Number(prompt("Введите второе число"));
let sum=0;

if (numOne-numTwo > 0) {
    getSum (numTwo,numOne);
} else if (numOne-numTwo < 0) {
    getSum (numOne,numTwo);
} else {
    console.log (numOne);
}

function getSum (numOne,numTwo) {
    while (numOne<numTwo+1) {
        sum = sum + numOne;
        numOne++;
    }
    console.log (sum);
}

//Напишите функцию fooboo которая принимает в качестве аргумента три параметра:

value = false;

console.log (value==true);
function fooboo (value, foo, boo) {
    if (value == true) {
        foo();
    } else {
        boo();
    }    
}

function foo () {
    console.log("Отработала Foo");
}

function boo () {
    console.log("Отработала Boo");
}

fooboo (value, foo, boo);

///Напишите функцию withNumberArgs(), которая будет декорировать любую функцию от двух переменных, которую вы в неё передадите.
///Декорированная функция должна выводить в консоль ошибку, если тип одного из аргументов не равен числу, и возвращать 0:
function withNumberArgs(mul,isNumber) {
    return function (a,b) {
        if (isNumber(a) == true && isNumber(b) == true){
            return mul(a,b);
        } else {
            return 0;
        }
    }
}

function isNumber(num) {
    return typeof num === 'number' && !isNaN(num) && num>0;
}
const mul = (a, b) => a * b;
const safeMul = withNumberArgs(mul,isNumber);
safeMul(5, 5) 
safeMul(5, "5") 

//Реализуйте функцию, который принимает 3 целочисленных значения a, b, c. Функция должна возвращать true, если треугольник можно построить со сторонами заданной длины, и false в любом другом случае.
const a = Number(prompt("Введите длину стороны A треугольника"));
const b = Number(prompt("Введите длину стороны B треугольника"));
const c = Number(prompt("Введите длину стороны C треугольника"));

function triangle (a,b,c) {
    if ((a+b>c) && (a+c>b) && (b+c>a)) {
        return true;        
    } else {
        return false;
    }    
}

console.log (triangle (a,b,c));

//Напишите программу для вычисления общей стоимости покупки телефонов. 



const nds = 20;
const balans = Number(prompt("Сколько денег на Вашем счету?"));
let newBalans = 0;
let profit = 0;

const iphone = {
    name: 'iphone',
    price: 2500,
    case: 40,
    charger: 80,
    salePrice: 2900
}

const samsung = {
    name: 'samsung',
    price: 2000,
    case: 35,
    charger: 40,
    salePrice: 2200
}

const xiaomi = {
    name: 'xiaomi',
    price: 1500,
    case: 30,
    charger: 25,
    salePrice: 1650
}

const huawei = {
    name: 'huawei',
    price: 1250,
    case: 30,
    charger: 25,
    salePrice: 1380
}


function pricePhone (phone,nds) {
    return (phone.price+phone.case+phone.charger)*(nds/100+1);
}

function salePrice (phone,nds) {
    return phone.salePrice*(nds/100+1);
}

function priceProfit (balans,phone,nds,pricePhone,salePrice) {
    profit = 0;
    newBalans = balans;
    while (newBalans>pricePhone(phone,nds)) {
        profit = profit + salePrice(phone,nds) - pricePhone(phone,nds);
        newBalans = newBalans -pricePhone(phone,nds);
    }
    console.log (`Если закупить телефоны ${phone.name} на сумму ${balans} максимальный профит после продажи составит: ${profit}`);
}

for (i=1;i<5;i++) {
    switch (i) {
        case 1: priceProfit (balans,iphone,nds,pricePhone,salePrice)
        break;
        case 2: priceProfit (balans,samsung,nds,pricePhone,salePrice)
        break;
        case 3: priceProfit (balans,xiaomi,nds,pricePhone,salePrice)
        break;
        case 4: priceProfit (balans,huawei,nds,pricePhone,salePrice)
        break;
    }

}


//Ваша задача - разбить плитку шоколада заданного размера n x m на маленькие квадраты. 

const x = Number(prompt("Размер шоколадки в ширину"));
const y = Number(prompt("Размер шоколадки в длину"));

function isNumber(num) {
    return typeof num === 'number' && !isNaN(num) && num>0;
}

function chocolate (x,y) {
    if (isNumber(x) == true && isNumber(y) == true){
       return (x-1)+(y-1)*(x); 
    } else {
        return 0;
    }
    
}

console.log (chocolate(x,y));
